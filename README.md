ic_msg
============

Custom message package for ROS

- MatF32
for opencv mat float mat
```
// For publish
ic_msg::Mat32F ros_mat;
cv::Mat cv_mat(2,2,CV_32FC1);

ros_mat.mat.assign((float*)cv_mat.datastart, (float*)cv_mat.dataend);
ros_mat.cn = cv_mat.channels();
ros_mat.cols = cv_mat.cols;
ros_mat.rows = cv_mat.rows;

// For subscribe
ic_msg::Mat32FConstPtr msg;
cv::Mat cv_mat;
if (msg->cn == 1)
    cv_mat.create(msg->rows, msg->cols, CV_32FC1);
else if (msg->cn == 3)
    cv_mat.create (msg->rows, msg->cols, CV_32FC3);

::memcpy(cv_mat.data, msg->mat.data(), sizeof(float) * msg->mat.size());
```
