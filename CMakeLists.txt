cmake_minimum_required(VERSION 2.8.3)
project(ic_msg)

find_package(catkin REQUIRED COMPONENTS std_msgs message_generation)

add_message_files( DIRECTORY msg
  FILES Mat32F.msg
)

generate_messages(
  DEPENDENCIES
  std_msgs
)

catkin_package(
  CATKIN_DEPENDS message_runtime roscpp std_msgs
)